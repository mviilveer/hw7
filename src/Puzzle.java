// https://docs.oracle.com/javase/7/docs/api/java/util/Hashtable.html

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class Puzzle {

    private static class Solver {
        /**
         * Solutions
         */
        private ArrayList<Hashtable<String, Integer>> solutions = new ArrayList<>();

        /**
         * Current permutation assigned to characters
         */
        private Hashtable<String, Integer> characterToNumberMap = new Hashtable<>();

        /**
         * All unique characters
         */
        private ArrayList<String> uniqueCharacters = new ArrayList<>();
        /**
         * First word
         */
        private String first;
        /**
         * Second word
         */
        private String second;
        /**
         * Third word
         */
        private String third;

        /**
         * Only public method
         * @param first String
         * @param second String
         * @param third String
         */
        void solve(String first, String second, String third)
        {
            this.first = first;
            this.second = second;
            this.third = third;
            findUniqueCharacters();
            fillCharacterNumberMap();

            validate();

            findSolutions();
        }

        /**
         * Core method
         */
        private void findSolutions() {
            List<List<Integer>> permutations = permute();
            for (List<Integer> permutation : permutations) {
                for (int i = 0; i < uniqueCharacters.size(); i++) {
                    characterToNumberMap.put(uniqueCharacters.get(i), permutation.get(i));
                }
                check();
            }
            if (solutions.size() > 0) {
                System.out.println(solutions.get(0));
            }
            System.out.println("Total solutions:" + solutions.size());
        }

        /**
         * Validates that unique character size is not greater than 10
         */
        private void validate() {
            if (uniqueCharacters.size() > 10) {
                throw new RuntimeException("Solver supports up to 10 characters, " + uniqueCharacters.size() + " given");
            }
        }

        /**
         * Check if the given permutation is suitable
         */
        private void check() {
            // before decoding, check if the result validates
            boolean isEligible = characterToNumberMap.get(first.substring(0, 1)) != 0
                    && characterToNumberMap.get(second.substring(0, 1)) != 0;

             if (!isEligible) {
                return;
            }

            if (decode(first) + decode(second) == decode(third) && !solutions.contains(characterToNumberMap)) {
                Hashtable<String, Integer> solution = new Hashtable<>(characterToNumberMap);
                solutions.add(solution);
            }
        }

        /**
         * Decodes string to number
         * @param s String
         * @return int
         */
        private long decode(String s) {
            StringBuilder numberString = new StringBuilder();
            for (int i = 0; i < s.length(); i++) {
                numberString.append(Integer.toString(characterToNumberMap.get(s.substring(i, i + 1))));
            }
            return Long.parseLong(numberString.toString());
        }

        /**
         * Finds all unique characters from given words
         */
        private void findUniqueCharacters()
        {
            StringBuilder allWords = new StringBuilder();
            allWords.append(first);
            allWords.append(second);
            allWords.append(third);

            for (int i = 0; i < allWords.length(); i ++) {
                String currentLetter = allWords.substring(i, i + 1);
                if (!uniqueCharacters.contains(currentLetter)) {
                    uniqueCharacters.add(currentLetter);
                }
            }
        }

        /**
         * Fills character number map
         */
        private void fillCharacterNumberMap()
        {
            for (String uniqueCharacter : uniqueCharacters) {
                characterToNumberMap.put(uniqueCharacter, 0);
            }
        }

        /**
         * My idea is to permutate 0-9. Then I will just loop all the List items and wait until problem is solved
         * Source: https://codereview.stackexchange.com/questions/68716/permutations-of-any-given-numbers
         * A bit refactored but idea is all the same
         * @return List
         */
        List<List<Integer>> permute() {
            int[] numbers = new int[10];
            for(int i = 0; i < numbers.length; i++) {
                numbers[i] = i;
            }

            // we use a list of lists rather than a list of arrays
            // because lists support adding in the middle
            // and track current length
            List<List<Integer>> permutations = new ArrayList<>();
            // Add an empty list so that the middle for loop runs
            permutations.add(new ArrayList<>());

            for (int number : numbers) {
                // create a temporary container to hold the new permutations
                // while we iterate over the old ones
                List<List<Integer>> current = new ArrayList<>();
                for (List<Integer> permutation : permutations) {
                    for (int j = 0, n = permutation.size() + 1; j < n; j++) {
                        List<Integer> temp = new ArrayList<>(permutation);
                        temp.add(j, number);
                        current.add(temp);
                    }
                }
                permutations = new ArrayList<>(current);
            }

            return permutations;
        }

    }
    /**
     * Solve the word puzzle.
     *
     * @param args three words (addend1 addend2 sum)
     */
    public static void main(String[] args) {
        if (args.length != 3) {
            throw new RuntimeException("Need 3 arguments");
        }
        for(int i = 0; i < args.length; i++) {
            if (args[i].trim().isEmpty()) {
                throw new RuntimeException("Argument " + i + " is empty");
            }
            if (args[i].trim().length() > 18) {
                throw new RuntimeException("Argument '" + args[i].trim() + "' is too long. Maximum is 18 characters.");
            }
        }


        Solver solver = new Solver();
        solver.solve(args[0], args[1], args[2]);
    }
}
